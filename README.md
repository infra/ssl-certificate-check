**SSL Certificate Check**

The certificates for the sites listed in the file ssl_check_sites.txt will be checked weekly, and if the certificate is due to expire within 30 days, notifications will be sent via email and to the Slack channel #ssl-cert-alerts. Currently this runs Wednesday mornings. Please feel free to add sites to the list!
